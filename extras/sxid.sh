#!/bin/bash

# get the working irectory
dir=$(dirname $(readlink $(which "$0")))

# prepare the channels list
file=$(mktemp $TMPDIR/sxid.XXXXXX)
$dir/sxtv > $file
while true
do
    # get the channel
    # if hash xkbswitch 2>/dev/null; then xkbswitch -s 1; fi
    chan=$(cat $file| fzf | sed 's/.*\[\(.*\)\]$/\1/')
    # if hash xkbswitch 2>/dev/null; then xkbswitch -s 0; fi
    if [ $? -eq 1 ]
    then
        exit
    fi

    # get the links
    if [ -z $chan ]
    then
        exit 1
    else
        comm="xidel -s $chan -e //*[@id='links_block']/table[2]/tbody/tr[*]/td[*]/table/tbody/tr/td[6]/a/@href"
        while true
        do
            # reset the array of links
            chn=()
            # re-fill the array of links
            while IFS= read -r line
            do
                chn+=( "$line" )
            done < <($comm)
            # provide a menu if no links were collected
            if [ ${#chn[@]} -eq 0 ]
            then
                printf "\033[1;31mERROR: no iframes found. Press <s> to open Safari, <RETURN> to continue, or <ESC> to abort \033[0m" && read -n1 CONF
                if [ "$CONF" = "s" ]
                then
                    printf "\n"
                    open -a Safari "$chan"
                    break
                elif [ "$CONF" = "" ]
                then
                    printf "\n"
                    break
                fi
            else
                break
            fi
        done
    fi

    # clean and open the links
    for i in "${chn[@]}"
    do
        # inspect
        case $i in
            # check for being a common link
            http*)
                chan=$i
                ;;
            # check for lacking the protocol
            //*)
                chan='http:'$i
                ;;
            # check for being not a link (for debugging)
            *)
                echo "_""$i""_"
                continue
                ;;
        esac
        # check links for being a redirect
        if curl -skI "$chan" | grep -iq 'Location:'
        then
            chan=$(curl -skI "$chan" | grep Location: | cut -d' ' -f2)
        fi
        # check links for being an internal resource
        if echo $chan | grep -iq 'livetv.*webplayer'
        then
            # xpath 1
            chaz=$(curl -sk "$chan" | xidel -s --data=- -e '/html/body/table/tbody/tr[2]/td/table/tbody/tr/td/iframe/@src')
            # xpath 2
            if [ -z "$chaz" ]
            then
                chaz=$(curl -sk "$chan" | xidel -s --data=- -e '/html/body/table/tbody/tr[2]/td/table/tbody/tr/td/div/iframe/@src')
            fi
            # make sure no line feeds are present in links
            chan=${chaz//$'\n'/}
        fi
        use_player=false
        # inspect
        case $chan in
            # check for being a special TWITCH case
            http*://*twitch.tv*)
                chan='http://twitch.tv/'$(echo $chan | sed 's/.*channel=\([^&]*\)\&.*/\1/')
                use_player=true
                ;;
            # check for being a special YOUTUBE case
            http*://*youtu*)
                use_player=true
                ;;
            # check for being a common case
            http*)
                ;;
            # check for lacking the protocol (again)
            //*)
                chan='http:'$chan
                ;;
            # check for being not a link (for debugging)
            *)
                echo "_""$chan""_"
                continue
                ;;
        esac
        # play
        printf "\033[1;37mTrying stream $chan\n\033[0m"
        if $use_player
        then
            open -a mpv "$chan"
        else
            open -a Safari "$chan"
        fi
        # provide a menu
        printf "\033[1;32mWARNING: press <RETURN> to continue, or <ESC> to abort \033[0m" && read -n1 CONF
        if [ "$CONF" = "" ]
        then
            printf "\n"
            break
        fi
    done
done
rm $file
