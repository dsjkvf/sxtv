#!/bin/bash

dir=$(dirname $(readlink $(which "$0")))

if hash xkbswitch 2>/dev/null
then
    xkbswitch -s 1
fi

chan=$($dir/sxtv | fzf | sed 's/.*\[\(.*\)\]$/\1/')
if [ -z $chan ]
then
    exit 1
else
    $dir/sxpl "$chan"
fi


if hash xkbswitch 2>/dev/null
then
    xkbswitch -s 0
fi
