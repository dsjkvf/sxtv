#!/bin/bash

dir=$(dirname $(readlink $(which "$0")))
SECONDS=0
while true
do
    clear
    $dir/sxtv-plain
    z=$(TZ=EET date +"%H:%M")
    printf $z
    sleep "$((1800 - (SECONDS%10)))"
done
