#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding('utf8')

import site
site.addsitedir('/home/s/.local/lib/python2.7/site-packages')

import feedparser
from datetime import datetime, timedelta
import pytz

print ('Content-type: text/html\n\n')
print('<head>')
print('<title>SXTV</title>')
print('<link rel="stylesheet" href="sxtv.css" type="text/css">')
print('<link href="favicon.ico" rel="icon" type="image/x-icon" />')
print('</head>')
print('<body>')

u = 'http://cdn.livetv333.me/rss/upcoming_ru.xml'
f = feedparser.parse(u)
e = f.entries
x = ['товарищеск', 'волейбол', 'киберспорт', 'гандбол', 'молодежный', 'настольный']
t = datetime.utcnow()
l = pytz.timezone('Europe/Moscow')
t = t.replace(tzinfo=pytz.utc).astimezone(l)
n = t - timedelta(hours = 2)
n = n.replace(tzinfo=None)
N = t + timedelta(hours = 2)
N = N.replace(tzinfo=None)

for i in e:
    t = datetime.strptime(i.published[:-6], '%a, %d %b %Y %H:%M:%S')
    # Moscow doesn't switches to DST any more but we still do
    if not bool(pytz.timezone('Europe/Brussels').localize(datetime.now()).dst()):
        t = t - timedelta(hours = 1)
    if t > n and t < N:
        if not any(X in i.description.lower() for X in x) and not any(X in i.title.lower() for X in x):
            header = str(t)[11:-3] + " | " + i.description + ": " + '<a href="' + i.link + '">' + i.title + '</a><br>'
            print(header.replace('\n', ''))
